package fizzbuzz;

public class FizzBuzz {

	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			System.out.println(checkFizzBuzz(i));
		}
	}

	/*
	 * 引数numが3の倍数なら「Fizz」
	 * 5の倍数なら「Buzz」
	 * 3と5両方の倍数なら「FizzBuzz」
	 * それ以外ならそのまま数字を戻り値として返す
	 * 
	 * 引数 int
	 * 戻り値 String
	 */
	public static String checkFizzBuzz(int num) {
		String Fizz = "Fizz";
		String Buzz = "Buzz";
		String FizzBuzz = "FizzBuzz";

		if (num % 3 == 0 && num % 5 == 0) {
			return FizzBuzz;
		} else if (num % 3 == 0) {
			return Fizz;
		} else if (num % 5 == 0) {
			return Buzz;
		} else {
			return Integer.toString(num);
		}

	}

}
