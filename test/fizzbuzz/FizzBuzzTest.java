/**
 * 
 */
package fizzbuzz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author ikemoto.nanako
 *
 */
class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * {@link fizzbuzz.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */

	@Test
	void testFizz() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	void testMBuzz() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	void testFizzBuzz() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	void testReturnNumber() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	void testReturnNumberSecond() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}

}
